﻿using IronOcr;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using OCRFiles.Extensions;
using OCRFiles.Models;
using OCRFiles.Util;
using System;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using edu.stanford.nlp.pipeline;
using Console = System.Console;
using System.IO;
using System.Text.RegularExpressions;
using OCRFiles.Services;

namespace OCRFiles.Controllers
{
    public class HomeController : Controller
    {
        DataService DataService { get; set; }

        public HomeController()
        {
            DataService = new DataService();
        }

        public ActionResult Index()
        {
            ViewBag.Show = false;
            //Test();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UploadFile()
        {
            ViewBag.Show = Main.GetInstance().Show;
            ViewBag.IronPdf = Main.GetInstance().IronPDF;
            ViewBag.iTextSharp = Main.GetInstance().ItextSharp;
            ViewBag.Skills = Main.GetInstance().Information.Skills;
            ViewBag.Education = Main.GetInstance().Information.Educations;
            ViewBag.Jobs = Main.GetInstance().Information.Experiences;

            ViewBag.Exp = Main.GetInstance().JobsStr;
            ViewBag.Edu = Main.GetInstance().EducationStr;
            ViewBag.Ski = Main.GetInstance().SkillStr;

            ViewBag.IsSuccess = Main.GetInstance().IsSuccess;
            ViewBag.Message = Main.GetInstance().Message;


            return View();
        }


        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase fileBase, int option, string name = "")
        {
            if (DataService == null) DataService = new DataService();
            try
            {
                if (!fileBase.ContentType.Equals("application/pdf"))
                {
                    return RedirectToAction("Index");
                }
                var url = HttpContext.Server.MapPath("~");
                url = string.Format("{0}{1}{2}", url, "\\Curriculum\\", name.RenameFile());
                //url = string.Format("{0}{1}{2}", url, "Curriculum\\", name.RenameFile());
                FileHelper.GetInstance().SetPermissionDirectory(string.Format("{0}{1}", url, "\\Curriculum\\"));
                if (System.IO.File.Exists(url))
                {
                    FileHelper.GetInstance().SetPermissionFile(url);
                    System.IO.File.Delete(url);
                    fileBase.SaveAs(url);
                }
                else
                {
                    FileHelper.GetInstance().SetPermissionFile(url);
                    fileBase.SaveAs(url);
                }

                Main.GetInstance().Show = true;

                if (option == (int)EnumOptions.IronOcr)
                {
                    Main.GetInstance().IronPDF = FileHelper.GetInstance().OCR(url);
                    DataService.Trainer(Main.GetInstance().IronPDF, HttpContext);
                    string a = Main.GetInstance().IronPDF;
                }
                else
                {
                    Main.GetInstance().ItextSharp = FileHelper.GetInstance().ReadPdfFile(url);
                    string b = Main.GetInstance().ItextSharp;
                    DataService.Trainer(Main.GetInstance().ItextSharp, HttpContext);
                }

                if (!Main.GetInstance().IsSuccess)
                {
                    ViewBag.Msg = Main.GetInstance().Message;
                    return RedirectToAction("UploadFile");
                }
                return RedirectToAction("UploadFile");
            }
            catch (Exception e)
            {
                Log.WriteLog(this, e);
            }
            return View("Index");
        }
         
    }
}