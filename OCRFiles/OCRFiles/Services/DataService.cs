﻿using OCRFiles.Extensions;
using OCRFiles.Models;
using OCRFiles.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCRFiles.Services
{
    public class DataService
    {
        public DataService()
        {

        }

        /// <summary>
        /// Metodo que se usa para obtener la información del archivo
        /// </summary>
        /// <param name="text"></param>
        /// <param name="http"></param>
        public void Trainer(string text, HttpContextBase http)
        {
            //text = text.RemoveSpecialCharacters();
            if (string.IsNullOrEmpty(text))
            {
                Main.GetInstance().IsSuccess = false;
                Main.GetInstance().Message = "No se encontro información del archivo";
                return;
            }
            Main.GetInstance().Information = new Information();
            try
            {
                text = text.RemoveAccentsWithRegEx();
                string[] data = null;
                if (text.Contains("\n\r"))
                {
                    data = text.Replace("\n\r", "\n\r ").Split(new string[] { "\n\r" }, StringSplitOptions.None).RemoveEmpty();
                }
                else
                {
                    data = text.Replace("\n", "\n\r").Split(new string[] { "\r" }, StringSplitOptions.None).RemoveEmpty();
                }
                
                string pathEducation = http.Server.MapPath("~/Docs/Education.txt");
                FileHelper.GetInstance().SetPermissionFile(pathEducation);
                string pathJobs = http.Server.MapPath("~/Docs/Jobs.txt");
                FileHelper.GetInstance().SetPermissionFile(pathJobs);
                string pathSkills = http.Server.MapPath("~/Docs/Skills.txt");
                FileHelper.GetInstance().SetPermissionFile(pathSkills);
                string pathPosition = http.Server.MapPath("~/Docs/Position.txt");
                FileHelper.GetInstance().SetPermissionFile(pathPosition);
                string pathActivities = http.Server.MapPath("~/Docs/Activities.txt");
                FileHelper.GetInstance().SetPermissionFile(pathActivities);
                var infoEducation = System.IO.File.ReadLines(pathEducation).ToListWords();
                var infoJobs = System.IO.File.ReadLines(pathJobs).ToListWords();
                var infoSkills = System.IO.File.ReadLines(pathSkills).ToListWords();
                var infoPosition = System.IO.File.ReadLines(pathPosition).ToListWords();
                var infoActivities = System.IO.File.ReadLines(pathActivities).ToListWords();

                var job = data.OrderTextByCategory(infoJobs);
                var edu = data.OrderTextByCategory(infoEducation);
                var ski = data.OrderTextByCategory(infoSkills);

                job = job.UnionText();
                edu = edu.UnionText();
                ski = ski.UnionText();

                Information information = new Information();
                information.Experiences = job.SetExperiencesData();
                information.Educations = edu.SetEducationData();
                information.Skills = ski.SetSkillsData();

                Main.GetInstance().Information = information;
            }
            catch (Exception e)
            {
                Log.WriteLog(this, e);
            }
        }
    }
}