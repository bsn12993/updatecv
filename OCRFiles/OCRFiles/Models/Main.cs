﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCRFiles.Models
{
    public class Main
    {
        private Main()
        {

        }

        static Main()
        {

        }

        private static Main instance = new Main();
        public string IronPDF { get; set; }
        public string ItextSharp { get; set; }
        public List<Skills> Skills { get; set; } = new List<Skills>();
        public List<Experience> Jobs { get; set; } = new List<Experience>();
        public List<Education> Education { get; set; } = new List<Education>();
        public List<Word> Position { get; set; } = new List<Word>();
        public List<DateTime> Dates { get; set; }
        public List<Word> Activities { get; set; } = new List<Word>();
        public List<Experience> GetExperiences { get; set; } = new List<Experience>();
        public bool Show { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; } = string.Empty;

        public string JobsStr { get; set; }
        public string SkillStr { get; set; }
        public string EducationStr { get; set; }

        public Information Information { get; set; } = new Information();

        public List<string> lstJobs = new List<string>();
        public List<string> lstEducation = new List<string>();
        public List<string> lstSkills = new List<string>();

        public static Main GetInstance()
        {
            if (instance == null) return new Main();
            else return instance;
        }

    }
}