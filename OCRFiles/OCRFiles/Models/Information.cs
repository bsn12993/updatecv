﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCRFiles.Models
{
    public class Information
    {
        public List<Education> Educations { get; set; }
        public List<Experience> Experiences { get; set; }
        public List<Skills> Skills { get; set; }

        public Information()
        {
            Educations = new List<Education>();
            Experiences = new List<Experience>();
            Skills = new List<Skills>();
        }
    }
}