﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCRFiles.Models
{
    public class Model
    {
        public string IronPDF { get; set; }
        public string ItextSharp { get; set; }
        public bool Show { get; set; }
    }
}