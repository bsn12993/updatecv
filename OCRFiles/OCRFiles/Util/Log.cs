﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace OCRFiles.Util
{
    public class Log
    {
        /// <summary>
        /// Metodo se usa para escribir un log en caso de un error/exceptión
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        public static void WriteLog(object obj, Exception e)
        {
            string date = DateTime.Now.ToString("yyyyMMdd");
            string time = DateTime.Now.ToString("HH:mm:ss");
            string path = HttpContext.Current.Request.MapPath($"~/Log/{date}.txt");

            StreamWriter streamWriter = new StreamWriter(path, true);
            StackTrace stackTrace = new StackTrace();
            streamWriter.WriteLine(obj.GetType().FullName + " " + time); stackTrace.GetFrame(1).GetFileLineNumber();
            streamWriter.WriteLine("Line: " + stackTrace.GetFrame(1).GetFileLineNumber());
            streamWriter.WriteLine(stackTrace.GetFrame(1).GetMethod().Name + " - " + e.Message);
            streamWriter.WriteLine("");

            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}