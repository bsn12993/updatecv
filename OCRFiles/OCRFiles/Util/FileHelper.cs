﻿using IronOcr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Web;
using OCRFiles.Models;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.IO;

namespace OCRFiles.Util
{
    public class FileHelper
    {
        private FileHelper()
        {

        }

        static FileHelper()
        {

        }

        private static FileHelper fileHelper;

        public static FileHelper GetInstance()
        {
            if (fileHelper == null) return new FileHelper();
            else return fileHelper;
        }
        /// <summary>
        /// Metodo que establece permisos al archivo
        /// </summary>
        /// <param name="url"></param>
        public void SetPermissionFile(string url = "")
        {
            FileIOPermission filePermission = new FileIOPermission(FileIOPermissionAccess.AllAccess, url);
            filePermission.AddPathList(FileIOPermissionAccess.Write | FileIOPermissionAccess.Read, url);
            try
            {
                filePermission.Demand();
            }
            catch (SecurityException e)
            {
                Log.WriteLog(this, e);
            }
        }
        /// <summary>
        /// Metodo que establece permisos al directorio
        /// </summary>
        /// <param name="url"></param>
        public void SetPermissionDirectory(string url = "")
        {
            FileIOPermission fileIOPermission = null;
            fileIOPermission = new FileIOPermission(FileIOPermissionAccess.AllAccess, url);
            fileIOPermission.Demand();
        }

        /// <summary>
        /// Lectura de archivo con itextsharp
        /// </summary>
        /// <param name="Filename"></param>
        /// <returns></returns>
        public string ReadPdfFile(object Filename)
        {
            string strText = string.Empty;
            try
            {
                using (PdfReader reader2 = new PdfReader((string)Filename))
                {
                    for (int page = 1; page <= reader2.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                        using (PdfReader reader = new PdfReader((string)Filename))
                        {
                            string s = PdfTextExtractor.GetTextFromPage(reader, page, its);
                            s = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(s)));
                            strText = strText + s;
                            reader.Close();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(strText)) Main.GetInstance().IsSuccess = true;
                else throw new Exception("No se pudo recuperar la información del archivo");
            }
            catch (Exception e)
            {
                Main.GetInstance().IsSuccess = false;
                Main.GetInstance().Message = e.Message;
                Log.WriteLog(this, e);
                return "";
            }
            return strText;
        }

        /// <summary>
        /// Lectura con IronOCR
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string OCR(string url = "")
        {
            var Text = string.Empty;
            try
            {
                //var Ocr = new AutoOcr()
                //{
                //    Language = IronOcr.Languages.Spanish.OcrLanguagePack
                //};

                var ocrReader = new AdvancedOcr()
                {
                    CleanBackgroundNoise = true,
                    EnhanceContrast = true,
                    EnhanceResolution = true,
                    ColorDepth = 4,
                    ColorSpace = AdvancedOcr.OcrColorSpace.Color,
                    DetectWhiteTextOnDarkBackgrounds = true,
                    RotateAndStraighten = true,
                    Language = IronOcr.Languages.Spanish.OcrLanguagePack,
                    InputImageType = AdvancedOcr.InputTypes.Document,
                    ReadBarCodes = true,
                    Strategy = AdvancedOcr.OcrStrategy.Advanced,
                };
                FileHelper.GetInstance().SetPermissionFile(url);
                //var PagesToRead = new[] { 1, 2, 3 };
                var result = ocrReader.ReadPdf(url);
                //var Results = Ocr.ReadPdf(url);
                var Barcodes = result.Barcodes;
                Text = result.Text;

                if (!string.IsNullOrEmpty(Text)) Main.GetInstance().IsSuccess = true;
                else throw new Exception("No se pudo recuperar la información del archivo");
            }
            catch (Exception e)
            {
                Main.GetInstance().IsSuccess = false;
                Main.GetInstance().Message = e.Message;
                Log.WriteLog(this, e);
                return "";
            }
            return Text;
        }

        /// <summary>
        /// Escribir texto en el archivo
        /// </summary>
        /// <param name="path"></param>
        /// <param name="lst"></param>
        /// <returns></returns>
        public bool IsWrite(string path, List<Word> lst)
        {
            StreamWriter streamWriter = new StreamWriter(path, true);
            try
            {
                foreach (var i in lst)
                {
                    streamWriter.WriteLine(i.Text.Trim());
                }
                return true;
            }
            catch(Exception e)
            {
                Log.WriteLog(this, e);
                return false;
            }
            finally
            {
                streamWriter.Flush();
                streamWriter.Close();
            }
        }

    }
}