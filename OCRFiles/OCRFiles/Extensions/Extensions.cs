﻿using OCRFiles.Models;
using OCRFiles.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace OCRFiles.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Metodo que convierte un lista de string a una lista de tipo de la clase Word
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<Word> ToListWords(this IEnumerable<string> value)
        {
            List<Word> words = new List<Word>();
            foreach (var i in value)
            {
                if (!string.IsNullOrEmpty(i))
                {
                    words.Add(new Word
                    {
                        Text = i
                    });
                }
            }
            return words;
        }

        public static string[] ReplaceCharacters(this string[] value)
        {
            for(int i = 0; i < value.Length; i++)
            {
                value[i] = value[i].Replace(" ", "");
            }
            return value;
        }

        /// <summary>
        /// Metodo que remueve los espacios en blanco
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string[] RemoveEmpty(this string[] value)
        {
            List<string> lst = new List<string>();
            for (int i = 0; i < value.Length; i++)
            {
                if (!string.IsNullOrEmpty(value[i].Trim())) lst.Add(value[i]);
            }
            return lst.ToArray();
        }

        public static void CreateFile(this string path)
        {
            if (!System.IO.File.Exists(path)) System.IO.File.Create(path);
        }

        public static string RenameFile(this string fileName)
        {
            return string.Format("{0}{1}{2}{3}", fileName, "", ".", "pdf");
        }

        /// <summary>
        /// Metodo que remueve los acentos del texto
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string RemoveAccentsWithRegEx(this string inputString)
        {
            Regex replace_a_Accents = new Regex("[á|à|ä|â]", RegexOptions.Compiled);
            Regex replace_e_Accents = new Regex("[é|è|ë|ê]", RegexOptions.Compiled);
            Regex replace_i_Accents = new Regex("[í|ì|ï|î]", RegexOptions.Compiled);
            Regex replace_o_Accents = new Regex("[ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex replace_u_Accents = new Regex("[ú|ù|ü|û]", RegexOptions.Compiled);
            inputString = replace_a_Accents.Replace(inputString, "a");
            inputString = replace_e_Accents.Replace(inputString, "e");
            inputString = replace_i_Accents.Replace(inputString, "i");
            inputString = replace_o_Accents.Replace(inputString, "o");
            inputString = replace_u_Accents.Replace(inputString, "u");
            return inputString;
        }

        /// <summary>
        /// Metodo que separa las frase, oraciones en base a las palabras claves para obtener la categoría correspondiente
        /// </summary>
        /// <param name="val"></param>
        /// <param name="words"></param>
        /// <param name="cat"></param>
        /// <returns></returns>
        public static string OrderTextByCategory(this string[] val, List<Word> words)
        {
            string str = "";
            Regex regex = null;
            foreach(var i in val)
            {
                foreach (var w in words)
                {
                    regex = new Regex("\\b" + w.Text.Trim().ToLower() + "\\b"); 
                    var result = regex.Matches(i.ToLower());
                    if (result.Count > 0)   
                    {
                        str += " " + i.Trim().Replace("\r\n", " ");
                        if (str.Substring(str.Length - 1).Contains(".")) str += "&&";
                        else str += ".&&";
                        break;
                    }
                }
            }
            return str;
        }

        /// <summary>
        /// Metodo que une y arma el texto que normalmente se recupera de manera desordenada por los saltos de linea.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string UnionText(this string value)
        {
            StringBuilder stringBuilder = new StringBuilder();
            var arr = value.Replace("&&", "&&#").Split(new string[] { "&&" }, StringSplitOptions.None);
            foreach(var i in arr)
            {
                var lastWord = string.Empty;
                if (stringBuilder.ToString().Trim().Length == 0) stringBuilder.Append(i.Trim());
                else
                {
                    if (i.Contains("."))
                    {
                        var point = i.IndexOf(".");
                        var len = i.Length - 1;
                        if (point == len) stringBuilder.Append(i.Trim()).AppendLine();
                        else stringBuilder.Append(" ").Append(i.Trim());
                    }
                    else stringBuilder.Append(" ").Append(i.Trim());
                }
            }
            var s = stringBuilder.ToString();
            return stringBuilder.ToString().Replace("\n", " ").Replace("?", "*");
        }

        /// <summary>
        /// Metodo que establece y almancena la información categorizada en listas de tipo de clase Education
        /// </summary>
        /// <param name="value"></param>
        public static List<Education> SetEducationData(this string value)
        {
            List<Education> educations = new List<Education>();
            var arr = value.Split(new string[] { "#" }, StringSplitOptions.None);
            foreach(var i in arr)
            {
                if (!string.IsNullOrEmpty(i.Trim()))
                {
                    var val = educations.Where(x => x.Text.Contains(i.Trim())).Count();
                    if (val == 0) 
                    {
                        educations.Add(new Education
                        {
                            Text = i.Trim()
                        });
                    }
                }
            }
            return educations;
        }

        /// <summary>
        /// Metodo que establece y almancena la información categorizada en listas de tipo de clase Skills
        /// </summary>
        /// <param name="value"></param>
        public static List<Skills> SetSkillsData(this string value)
        {
            List<Skills> skills = new List<Skills>();
            var arr = value.Split(new string[] { "#" }, StringSplitOptions.None);
            foreach (var i in arr)
            {
                if (!string.IsNullOrEmpty(i.Trim()))
                {
                    var val = skills.Where(x => x.Text.Contains(i.Trim())).Count();
                    if (val == 0)
                    {
                        skills.Add(new Skills
                        {
                            Text = i.Trim()
                        });
                    }

                }
            }
            return skills;
        }

        /// <summary>
        /// Metodo que establece y almancena la información categorizada en listas de tipo de clase Experience
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<Experience> SetExperiencesData(this string value)
        {
            List<Experience> experiences = new List<Experience>();
            Experience experience = null;
            var arr = value.Split(new string[] { "#" }, StringSplitOptions.None);
            foreach (var i in arr)
            {
                if (!string.IsNullOrEmpty(i.Trim()))
                {
                    var val = experiences.Where(x => x.Text.Contains(i.Trim())).Count();
                    if (val == 0)
                    {
                        experiences.Add(new Experience
                        {
                            Text = i.Trim()
                        });
                    }
                }
            }
            return experiences;
        }

        
        public static string RemoveSpecialCharacters(this string value)
        {
            try
            {
                return Regex.Replace(value, @"^\\£I=li£=…", " ", RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            catch(RegexMatchTimeoutException e)
            {
                return string.Empty;
            }
        }
        
    }
}